-- see `:help` for any questions
-- use `&<var>` to show value of vimscript variable


-- API --
-- o     = vim.o     -- options
-- go    = vim.go    -- only-global options
-- bo    = vim.bo    -- buffer local options
-- wo    = vim.wo    -- window local options

-- cmd   = vim.cmd   -- vim commands
-- fn    = vim.fn    -- vim functions
-- opt   = vim.opt   -- vim option object

-- g     = vim.g     -- global variables
-- b     = vim.b     -- buffer local variables
-- w     = vim.w     -- window local variables
-- t     = vim.t     -- tab local variables
-- v     = vim.v     -- variables
-- env   = vim.env   -- environment variables

require("native-lsp/nvim-lsp-installer")

local lspconfig = require('lspconfig')

local on_attach = function(_, bufnr)
    vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')
    require('completion').on_attach()
end

local servers = {'zls'}
for _, lsp in ipairs(servers) do
    lspconfig[lsp].setup {
        on_attach = on_attach,
    }
end

opt.completeopt = "menu,menuone,noselect"
g.completion_enable_auto_popup = 1

require("native-lsp/nvim-cmp")
