local lsp_installer = require("nvim-lsp-installer")


local lang_servers = {
    "bashls",
    "ccls",
    "clangd",
    "cmake",
    "cssls",
    "cssmodules_ls",
    "diagnosticls",
    "dockerls",
    "dotls",
    "efm",
    --"eslint",
    "emmet_ls",
    --"grammarly",
    "graphql",
    "html",
    --"hls",
    "jsonls",
    --"jdtls",
    --"quick_lint_js",
    "tsserver",
    --"ltex",
    "texlab",
    "sumneko_lua",
    "remark_ls",
    --"zk",
    "puppet",
    --"jedi_language_server",
    "pyright",
    --"pylsp",
    "sqlls",
    --"sqls",
    --"svelte",
    "taplo",
    --"tailwindcss",
    --"tflint", -- going to use tsserver first
    "lemminx",
    "yamlls",
    "zls",
}

for _, name in pairs(lang_servers) do
    local server_is_found, server = lsp_installer.get_server(name)
    if server_is_found then
        if not server:is_installed() then
            print("Installing " .. name)
            server:install()
        end
    end
end



lsp_installer.on_server_ready(function(server)
    local opts = {}
    opts.on_attach = on_attach
    opts.capabilities = require("cmp_nvim_lsp").update_capabilities(vim.lsp.protocol.make_client_capabilities())

    server:setup(opts)
    vim.cmd [[ do User LspAttachBuffers ]]
end)


lsp_installer.settings({
    ui = {
        icons = {
            server_installed = "✓",
            server_pending = "➜",
            server_uninstalled = "✗"
        }
    }
})
