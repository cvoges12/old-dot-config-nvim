local cmp = require("cmp")
local luasnip = require("luasnip")

if not luasnip then
  return
end

local lsp_symbols = {
  Text          = "   (Text) ",
  Method        = "   (Method)",
  Function      = "   (Function)",
  Constructor   = "   (Constructor)",
  Field         = " ﴲ  (Field)",
  Variable      = "   (Variable)",
  Class         = "   (Class)",
  Interface     = "   (Interface)",
  Module        = "   (Module)",
  Property      = " 襁 (Property)",
  Unit          = "   (Unit)",
  Value         = "   (Value)",
  Enum          = " 練 (Enum)",
  Keyword       = "   (Keyword)",
  Snippet       = "   (Snippet)",
  Color         = "   (Color)",
  File          = "   (File)",
  Reference     = "   (Reference)",
  Folder        = "   (Folder)",
  EnumMember    = "   (EnumMember)",
  Constant      = "   (Constant)",
  Struct        = "   (Struct)",
  Event         = "   (Event)",
  Operator      = "   (Operator)",
  TypeParameter = "   (TypeParameter)"
}

local has_words_before = function()
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

cmp.setup({
    snippet = {
        expand = function(args)
            luasnip.lsp_expand(args.body)
        end,
    },

    mapping = {
        ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
        ["<C-n>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_next_item()
            elseif luasnip.expand_or_jumpable() then
                luasnip.expand_or_jump()
            elseif has_words_before() then
                cmp.complete()
            else
                fallback()
            end
        end, { "i", "s" }),
        ["<C-e>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_prev_item()
            elseif luasnip.jumpable(-1) then
                luasnip.jump(-1)
            else
                fallback()
            end
        end, { "i", "s" }),
        ['<C-CR>'] = cmp.mapping.confirm {
            behavior = cmp.ConfirmBehavior.Replace,
            select = true,
        },
    },

    formatting = {
        format = function(entry, item)
            item.kind = lsp_symbols[item.kind] .. " " .. item.kind
            -- set a name for each source
            item.menu = ({
                spell = "[Spell]",
                buffer = "[Buffer]",
                calc = "[Calc]",
                emoji = "[Emoji]",
                nvim_lsp = "[LSP]",
                path = "[Path]",
                look = "[Look]",
                treesitter = "[treesitter]",
                luasnip = "[LuaSnip]",
                nvim_lua = "[Lua]",
                latex_symbols = "[Latex]",
                cmp_tabnine = "[Tab9]"
            })[entry.source.name]
            return item
        end
    },

    sources = {
        { name = 'nvim_lsp' },
        { name = 'cmp_tabnine' },
        { name = 'luasnip' },
        { name = 'path' },
        { name = 'latex_symbols'},
        { name = 'treesitter' },
        { name = 'buffer' },
        {
            name = 'look',
            keyword_length = 2,
            option = {
                convert_case = true,
                loud = true
            }
        },
        { name = 'emoji' },
        { name = 'calc' }
    },

    cmp.setup.cmdline('-', {
        sources = {
            { name = 'path' },
            { name = 'cmdline' },
            { name = 'nvim_lua' },
            { name = 'buffer' }
        }
    }),

    cmp.setup.cmdline('/', {
        sources = {
            { name = 'buffer' }
        }
    }),

    cmp.setup.cmdline(':', {
        sources = cmp.config.sources({
            { name = 'path' }
        }, {
            { name = 'cmdline' }
        })
    }),
})

local tabnine = require('cmp_tabnine.config')

tabnine:setup({
    max_lines = 1000;
    max_num_results = 20;
    sort = true;
    run_on_every_keystroke = true;
    snippet_placeholder = '..';
    ignored_file_types = { -- default is not to ignore
        -- uncomment to ignore in lua:
        -- lua = true
    };
})
