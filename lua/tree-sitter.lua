cmd [[
TSInstall bash
TSInstall bibtex
TSInstall c
TSInstall cmake
TSInstall cpp
TSInstall css
TSInstall dockerfile
TSInstall dot
TSInstall graphql
TSInstall html
TSInstall http
TSInstall javascript
TSInstall latex
TSInstall llvm
TSInstall lua
TSInstall make
TSInstall python
TSInstall regex
TSInstall verilog
TSInstall vim
TSInstall yaml
TSInstall zig
TSUpdate
]]

