-- see `:help` for any questions
-- use `&<var>` to show value of vimscript variable

require('keymap/functional')

-- API --
-- o     = vim.o     -- options
-- go    = vim.go    -- only-global options
-- bo    = vim.bo    -- buffer local options
-- wo    = vim.wo    -- window local options

-- cmd   = vim.cmd   -- vim commands
-- fn    = vim.fn    -- vim functions
-- opt   = vim.opt   -- vim option object

-- g     = vim.g     -- global variables
-- b     = vim.b     -- buffer local variables
-- w     = vim.w     -- window local variables
-- t     = vim.t     -- tab local variables
-- v     = vim.v     -- variables
-- env   = vim.env   -- environment variables


local layouts = {
    'qwerty',
    'colemak'
}
local layout = layouts[2]

local map = vim.api.nvim_set_keymap

local function modes_map(modes)
    local functions = {}
    for _,mode in pairs(modes) do
        table.insert(functions, bind(map,mode))
    end
    return functions
end

local function keys_modes_map(modes, new_keys, old_keys)
    for i = 1, #new_keys do
        for _,f in pairs(modes_map(modes)) do
            f(new_keys[i], old_keys[i], {noremap = true})
        end
    end
end

local nvo = {'n','v','o'}

if layout == 'qwerty' then
    --
    -- Qwerty: General Bindings
    --

    -- Splits
    for _,key in pairs({'j','k','l','J','K','L'}) do
        for _,f in pairs(modes_map({'n', 'v'})) do
            f('<Leader>w'..key, '<C-W>'..key, {noremap = true})
        end
    end

    --
    -- Qwerty: Plugin Bindings
    --

    -- NERDTree                                     defaults
    vim.g.NERDTreeMapOpenSplit          = 'i'       -- i
    vim.g.NERDTreeMapPreviewSplit       = 'I'       -- gi
    vim.g.NERDTreeMapJumpFirstChild     = 'K'       -- K
    vim.g.NERDTreeMapJumpLastChild      = 'J'       -- J
    vim.g.NERDTreeMapJumpNextSibling    = '<C-j>'   -- <C-J>.
    vim.g.NERDTreeMapJumpPrevSibling    = '<C-k>'   -- <C-K>
    vim.g.NERDTreeMapChangeRoot         = 'l'       -- C
    -- see "All layout" bindings too

elseif layout == 'colemak' then
    --
    -- Colemak: General Bindings
    --

    -- Motion
    -- up(e) down(n) left(h) right(i)
    keys_modes_map(
        nvo,
        {'h','n','e','i'},
        {'h','j','k','l'}
    )

    -- word(y) WORD(Y) back(l) BACK(L) end(u) END(U)
    keys_modes_map(
        nvo,
        {'l','L','u','U','gu','gU','y','Y'},
        {'b','B','e','E','ge','gE','w','W'}
    )

    -- (a)ppend (r)eplace in(s)ert change(w)
    keys_modes_map(
        {'n'},
        {'s','S'},  -- see Visual Line Mode Patch
        {'i','I'}
    )
    keys_modes_map(
        nvo,
        {'w','W'},
        {'c','C'}
    )

    -- (c)opy (p)aste cut(x,d)
    keys_modes_map(
        nvo,
        {'c','C'},
        {'y','Y'}
    )

    -- (u)ndo (r)edo
    local c_undo_redo = {'z','gz','Z'}
    keys_modes_map(
        {'n'},
        c_undo_redo,
        {'u','U','<C-R>'}
    )
    local q_x_undo = ':<C-U>undo<CR>'
    keys_modes_map(
        {'x'},
        c_undo_redo,
        {
            q_x_undo,
            q_x_undo,
            ':<C-U>redo<CR>'
        }
    )

    -- Visual Line Mode Patch
    vim.cmd([[
        xnoremap <silent> <expr> s (mode() =~# "[V]" ? "\<C-V>0o$I" : "I")
        xnoremap <silent> <expr> S (mode() =~# "[V]" ? "\<C-V>0o$I" : "I")
    ]])

    -- (g)oto
    -- ge = visual line up
    -- gn = visual line down
    keys_modes_map(
        nvo,
        {'ge','gn'},
        {'gk','gj'}
    )

    -- Search
    -- (f)ind (F)ind (t)il (T)il next(k) prev(K)
    keys_modes_map(
        nvo,
        {'k','K'},
        {'n','N'}
    )

    -- Text Objects
    -- inner(s) (a)
    map('o', 's', 'i', {noremap = true})

    -- Folds
    keys_modes_map(
        {'n','x'},
        {'j','jn','je','jo','jc','ja'},
        {'z','zn','ze','zo','zc','za'}
    )

    -- Help(B)
    map('n', 'B', 'K', {noremap = true})

    -- Splits
    local split = {
        colemak = {'n','e','i','N','E','I'},
        qwerty  = {'j','k','l','J','K','L'}
    }
    for i = 1, #split.colemak do
        for _,f in pairs(modes_map({'n','v'})) do
            f('<Leader>w'..split.colemak[i], '<C-W>'..split.qwerty[i], {noremap = true})
        end
    end

    -- Screen / Scroll / Page
    -- H = top screen
    -- M = mid screen
    -- I = bottom screen
    -- E = half page up
    -- N = half page down
    keys_modes_map(
        {'n','v'},
        {'I','E',    'N'},
        {'L','<C-U>','<C-D>'}
    )

    --
    -- Colemak: Plugin Bindings
    --

    -- NERDTree                                     defaults
    vim.g.NERDTreeMapOpenSplit          = 's'       -- i
    vim.g.NERDTreeMapPreviewSplit       = 'S'       -- gi
    vim.g.NERDTreeMapJumpFirstChild     = 'E'       -- K
    vim.g.NERDTreeMapJumpLastChild      = 'N'       -- J
    vim.g.NERDTreeMapJumpNextSibling    = '<C-n>'   -- <C-J>.
    vim.g.NERDTreeMapJumpPrevSibling    = '<C-e>'   -- <C-K>
    vim.g.NERDTreeMapChangeRoot         = 'i'       -- C
    -- see "All layout" bindings too

end

--
-- All layouts: General Bindings
--

-- Splits
for _,key in pairs({'h','H','t','q',''}) do
    for _,f in pairs(modes_map({'n', 'v'})) do
        if key ~= 't' then
            f('<Leader>w'..key, '<C-W>'..key, {noremap = true})
        else
            f('<Leader>wt', '<C-W>T', {noremap = true})
        end
    end
end

-- Search
keys_modes_map(
    {'n','v'},
    {'/',  '?'},
    {'q/i','q?i'}
)

-- Remove left over search highlights
map('n', '<Leader><space>', ':nohlsearch<CR>', {noremap = true})

-- Move current line to center of screen
map('n', '<Leader>m', 'z.', {noremap = true})

-- Buffers
local buffer_keys = {
    new = {'<tab>','<S-tab>','<BS>'},
    old = {'n',    'p',     'd'}
}
for i = 1, #buffer_keys.new do
    for _,f in pairs(modes_map({'n','v','x'})) do
        f(
            '<Leader>'..buffer_keys.new[i],
            ':b'..buffer_keys.old[i]..'<CR>',
            {noremap = true}
        )
    end
end

-- Command mode
keys_modes_map(
    {'n', 'v', 'x'},
    {':',  ';;',  ';e',          ';h',           ';v'},
    {'q:i','q:i!','q:ie term://','q:isp term://','q:ivs term://'}
)

-- Filetype specific
cmd('autocmd FileType zig lua zig_maps()')
function zig_maps()
    map('n', '<Leader>ft', ':vertical !zig test %<CR>', {noremap = true})
    map('n', '<Leader>fr', ':vertical !zig run %<CR>', {noremap = true})
end

--
-- All layouts: Plugin Bindings
--

-- Package manager
function pm_sync()
    require('plugins')
    cmd('PaqSync')
end
map('n', '<leader>p', ':lua pm_sync()<CR>', {noremap = true})

-- LSP
local lsp_commands = {
    new = {'h','d','D','r','f','n','i','s','S','<','>','a'},
    old = {
        'buf.hover',            -- h
        'buf.definition',       -- d
        'buf.declaration',      -- D
        'buf.references',       -- r
        'buf.formatting',       -- f
        'buf.rename',           -- n
        'buf.implementation',   -- i
        'buf.document_symbol',  -- s
        'buf.signature_help',   -- S
        'diagnostic.goto_prev', -- <
        'diagnostic.goto_next', -- >
        'buf.code_action'       -- a
    }
}
for i = 1, #lsp_commands.new do
    for _,f in pairs(modes_map({'n'})) do
        f('<Leader>l'..lsp_commands.new[i],
            '<cmd>lua vim.lsp.'..lsp_commands.old[i]..'()<CR>',
            {noremap = true}
        )
    end
end
