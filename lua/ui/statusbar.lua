-- see `:help` for any questions
-- use `&<var>` to show value of vimscript variable

-- API --
-- o     = vim.o     -- options
-- go    = vim.go    -- only-global options
-- bo    = vim.bo    -- buffer local options
-- wo    = vim.wo    -- window local options

-- cmd   = vim.cmd   -- vim commands
-- fn    = vim.fn    -- vim functions
-- opt   = vim.opt   -- vim option object

-- g     = vim.g     -- global variables
-- b     = vim.b     -- buffer local variables
-- w     = vim.w     -- window local variables
-- t     = vim.t     -- tab local variables
-- v     = vim.v     -- variables
-- env   = vim.env   -- environment variables


--Later generalize into plugin
-- read from bottom up to understand `status_bar()`

local function file_state()
    if vim.bo.modified then
        return "+"
    elseif vim.bo.readonly then
        return "-"
    end
    return ""
end

--local function diagnostics()
--    --local buffer_number = vim.fn.bufnr('%')
--    local severity_levels = {
--                    -- level,prefix
--        errors      = {'Error', 'E:'},
--        warnings    = {'Warning', 'W:'},
--        info        = {'Information', 'I:'},
--        hints       = {'Hint', 'H:'}
--    }
--    local out = ''
--    for _,v in pairs(severity_levels) do
--        local d = vim.diagnostic.get(
--            0,
--        )
--        --local d = vim.lsp.diagnostic.get_count(
--        --    buffer_number,
--        --    v[1] -- level
--        --)
--        if d > 0 then
--            out = out .. v[2] .. d .. ' '
--        end
--    end
--    return out
--end

local function highlight(group, color)
    cmd('highlight ' .. group .. ' cterm='..color .. ' gui='..color)
end

highlight('StatusLine', 'bold,reverse')
highlight('StatusLineDark', 'bold')

local light_highlight   = '%#StatusLine#'
local dark_highlight    = '%#StatusLineDark#'

function section(items)
    local out = ''
    for _,item in pairs(items) do
        if item ~= '' and item ~= nil then
            if out == '' then
                out = item
            else
                out = out .. ' | ' .. item
            end
        end
    end
    return ' ' .. out .. ' '
end

function sections(items)
    local out = {}
    local n = 1
    for _,item in pairs(items) do
        if item == '%=' then
            table.insert(out, item)
            n = 1 -- reset coloring at different parts
        else
            if (n % 2) == 1 then
                table.insert(out, light_highlight..section(item))
            else
                table.insert(out, dark_highlight..section(item))
            end
            n = n + 1
        end
    end
    return out
end

function status_bar()
    return table.concat(sections({
        -- Stage Left
        {'%f', file_state()},
        --{diagnostics()},
        '%=',
        -- Stage Right
        {
            vim.b.gitsigns_status,
            vim.bo.filetype
        },
        {'%p%%'},
        {'%c,%l'}
    }))
end
