# Vim Config

My vim config written in lua designed for *smol boi minimal vibes*

Most of the code is in separate modules. Each will later be its own true plugin.


### Installation

Install the package manager `paq` to start:

```sh
git clone --depth=1 https://github.com/savq/paq-nvim.git \
    "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/pack/paqs/start/paq-nvim
```

#### LSP servers

Scripts are supplied in `scripts/` for each server I use. Run these for the
corresponding server that you would like to be installed.

The process in no way is automated or "purely in lua or vimscript", but it more
just holds your hand a bit. There may be plans in the future to automate this
with our own package manager. But I might start writing my own editor by that
point. So we'll see.

If you'd like to add a new LSP server, feel free. Just make sure to ask the
user if all dependencies are installed. Currently, this is achieved using
`query_dep` in `query_dependency.sh`. All it does is ask the user if the
dependency is installed and quits the install script if any dependency is not
installed. *(don't even remind me how hacky that is)*

However, a proper package management system would be more prefered. So never
feel like this pattern is in any way enforced. I can't be biased toward which
packages for which package managers will get maintained. And I can't be
bothered to write a package for every package manager that ever exists. If it
bothers you enough, change it to specifically work with your system. And maybe
contribute so others maybe follow suit.


### Configure

Make sure to set your layout in `keymap/init.lua`
to whichever layout you prefer.
